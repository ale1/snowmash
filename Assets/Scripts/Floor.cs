﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour
{

    public float width;
    public float length;
    public float LeftEdge;
    public float RightEdge;

    public Block parentBlock { get; private set; }


    public void Setup()
    {
        MeshRenderer mesh = GetComponent<MeshRenderer>();
        width = mesh.bounds.size.x;
        length = mesh.bounds.size.z;
        float laneWidth = width / 5;
        LeftEdge = transform.localPosition.x - width / 2;
        RightEdge = transform.localPosition.x + width / 2;

        

        #if UNITY_EDITOR
        if (this.gameObject.layer != 8)
            Debug.LogError(this.name + "this floor is not in layer 8");
        #endif
    }


    private void Awake()
    {
      
    }



   
    // Start is called before the first frame update
    void Start()
    {
        parentBlock = GetComponentInParent<Block>();

    }

    // Update is called once per frame
    void Update()
    {


    }


    public void ActivateBlock()
    {
        parentBlock.ActivateBlock();
    }
}
