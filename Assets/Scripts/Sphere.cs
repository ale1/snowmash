﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sphere : MonoBehaviour
{
    public Snowball snowball { get; private set; }

    public float Radius
    {
        get
        {
            return GetComponent<Renderer>().bounds.extents.magnitude / 2f;  //todo guardar renderer. 
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        snowball = GetComponentInParent<Snowball>();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
