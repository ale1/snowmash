﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Route
{

    public List<Vector3> waypoints { get; private set; }
    public List<Vector3> unspentWaypoints { get; private set; }
    public List<Vector3> spentWaypoints { get; private set; }
    public Vector3 Rotation { get; private set; }


    //cons
    public Route(List<Vector3> _waypoints, Vector3 _targetRotation)
    {
        waypoints = new List<Vector3>(_waypoints);
        Rotation = _targetRotation;
        unspentWaypoints = new List<Vector3>(_waypoints);
        spentWaypoints = new List<Vector3>();
    }


    public bool Empty
    {
        get
        {
            return (unspentWaypoints.Count == 0);
        }
    }

    public float Distance
    { 
        get
        {
            float distance = 0;
            for(int i=0;i< waypoints.Count - 1;i++)
            {
                distance += Vector3.Distance(waypoints[i], waypoints[i + 1]);
            }
            return distance;
        }
    }

    public Vector3 Midpoint
    {
        get { return waypoints[waypoints.Count / 2]; }
    }




    public float Progress
    {
        get {
            float progress = (float) spentWaypoints.Count / (float) waypoints.Count;
            #if UNITY_EDITOR
            if (progress > 1 || progress < 0)
                Debug.LogError("why is route progress: " + progress +" = "+ spentWaypoints.Count + " / " + waypoints.Count + " ------ " + unspentWaypoints.Count );
            #endif
            return progress;
        }
    }

    public Vector3 Next()
    {
        return unspentWaypoints[0];
    }

    public void Clean()
    {
        spentWaypoints.Add(unspentWaypoints[0]);
        unspentWaypoints.RemoveAt(0);
    }
}


