﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelController : MonoBehaviour
{

    private Panel activePanel;

    private HomePanel HomePanel;
    private SelectionPanel LevelSelectPanel;
    private WinPanel WinPanel;
    private LosePanel LosePanel;
    private PlayPanel PlayPanel;


    // Start is called before the first frame update
    void Start()
    {
        HomePanel = GetComponentInChildren<HomePanel>();
        WinPanel = GetComponentInChildren<WinPanel>();
        LevelSelectPanel = GetComponentInChildren<SelectionPanel>();
        LosePanel = GetComponentInChildren<LosePanel>();
        PlayPanel = GetComponentInChildren<PlayPanel>();

#if UNITY_EDITOR
        if (HomePanel == null || WinPanel == null || LevelSelectPanel == null || LosePanel == null || PlayPanel == null)
            Debug.LogError("no encontre uno de los panels");
        #endif

        ActivateHome();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ActivateLevelSelect()
    {
        if (activePanel == LevelSelectPanel)
            Debug.Log("hmmmmm");

        Activate(LevelSelectPanel);
    }

    public void ActivateHome()
    {
        if (activePanel == HomePanel)
            Debug.Log("hmmmmm");

        Activate(HomePanel);
    }


    public void ActivateWinPanel(ScoreKeeper _scoreKeeper)
    {
        
        if (activePanel == WinPanel)
            Debug.Log("hmmmmm");

        WinPanel.scoreKeeper = _scoreKeeper;
        if (_scoreKeeper == null)
            Debug.LogError("hmm");
        Activate(WinPanel);
       
    }

    public void ActivateLosePanel()
    {
        if (activePanel == LosePanel)
            Debug.Log("hmmmmm");

        Activate(LosePanel);
    }

    public void ActivatePlayPanel()
    {
        if (activePanel == PlayPanel)
            Debug.Log("hemmm");

        Activate(PlayPanel);
    }

    public void HideActivePanel()
    {
        activePanel.Hide();
    }


    private void Activate(Panel _panel)
    {
        

        if (activePanel != null)
        {
            //Debug.Log("deactivsting" + activePanel.name);
            activePanel.Hide();
            activePanel = null;
        }
        activePanel = _panel;
        activePanel.Show();
        //Debug.Log("activating:" + activePanel.name);
    }


}
