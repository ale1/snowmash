﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//TODO: que herede de enemy
public class Snowbiker : MonoBehaviour
{
    private ParticlePool particlePool;

    private Level currentLevel;
    public Transform container;

    private float speed = 200f;

    private bool isColliding;
    private bool sleep;
    private bool Finished;
    private Vector3 startPos;
    public Transform targetCrashSite;

    public int DetectionRange = 250;
    private int frameCounter = 0;
    private Snowball snowball;
    private Quaternion startRotation;


    // Start is called before the first frame update
    void Start()
    {
        sleep = true;
        particlePool = FindObjectOfType<ParticlePool>();
        snowball = FindObjectOfType<Snowball>();
        startPos = this.transform.position;
        currentLevel = GetComponentInParent<Level>();
        startRotation = this.transform.rotation;
        targetCrashSite.gameObject.GetComponent<Renderer>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (sleep)
        {
            frameCounter++;

            if (frameCounter > 4)
            {
                DetectSnowball(); //should I wake up?
                frameCounter = 0;
            }

        }


        if (sleep)
            return;


        if(!Finished)
        {
            float t = 1 - (targetCrashSite.position.z - snowball.transform.position.z) / DetectionRange;
            //this.transform.position = Vector3.Lerp(startPos, targetCrashSite.position, t);
            this.transform.rotation = Quaternion.Slerp(startRotation, Quaternion.LookRotation(targetCrashSite.position - transform.position), t);
            speed = 100f + 100 * t;
            transform.Translate(Vector3.forward * speed * Time.deltaTime);

            if (t >= 0.98f || Vector3.Distance(this.transform.position, targetCrashSite.position) < 5f)
            {
                Finished = true;
            }
               
        }
        else
        {
            this.transform.position += transform.forward * speed * Time.deltaTime;
            Destroy(this.gameObject, 3f);

        }     
    }



    public void OnTriggerEnter(Collider other)
    {

        if (sleep) return;
        if (isColliding) return;
        isColliding = true;

        Snowball hit = other.GetComponentInParent<Snowball>();

        if (hit != null)
        {
            snowball.GoodHit(10);
            Kill();
        }
        else
        {
            isColliding = false;
        }
    }

    private void Kill()
    {
        Destroy(container.gameObject);
    }

    private void DetectSnowball()
    {
        
        if (targetCrashSite.position.z - snowball.transform.position.z < DetectionRange)
        {
            sleep = false;
            
        }

    }


}
