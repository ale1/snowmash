﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Level : MonoBehaviour
{

    private Countdown countdown;

    public int id = -1;  

    public SpawnPoint SpawnPoint { get; private set; }

    public float levelTimer = 30;

    //ScoreKeeping
    public ScoreKeeper scoreKeeper;

    //Skybox
    float skyBoxTimer = 0;

    public Material SkyboxMatStart;
    public Material SkyboxMatEnd;

    private List<Block> sortedBlocks;


    void Awake()
    {
        if (id == -1)
            Debug.LogError("SalameException:: te olvidaste de setear el id del " + this.transform.gameObject.name);

        foreach(Floor floor in GetComponentsInChildren<Floor>())
        {
            floor.Setup();
        }
        foreach(Block block in GetComponentsInChildren<Block>())
        {
            block.Setup();
        }

        SpawnPoint = FindObjectOfType<SpawnPoint>();
        RenderSettings.skybox = new Material(SkyboxMatStart);
        skyBoxTimer = 0;
         

    }


    // Start is called before the first frame update
    void Start()
    {

        if (scoreKeeper == null)
            scoreKeeper = new ScoreKeeper(id);
        else
            scoreKeeper.SoftReset(); // resteamos item count pero no las estrellas ya obtenidas.

        scoreKeeper.SetMaxCount<Skier>(GetComponentsInChildren<Skier>(true).Length);
        scoreKeeper.SetMaxCount<Snowboarder>(GetComponentsInChildren<Snowboarder>(true).Length);
        scoreKeeper.SetMaxCount<Snowbiker>(GetComponentsInChildren<Snowbiker>(true).Length);

        countdown = FindObjectOfType<Countdown>();
        countdown.StartCountdown();

        SaveManager.Instance.savedata.scoreKeepers[id] = scoreKeeper;


        List<Block> blocks = GetComponentsInChildren<Block>().ToList();
        
        for (int i = 0; i < blocks.Count; i++)
        {
            blocks[i].Order = i;
        }

        sortedBlocks = blocks.OrderBy(x => x.Order).ToList();

        for (int i = 1; i < sortedBlocks.Count; i++)
        {
            Block previousBlock = sortedBlocks[i - 1];
            float localHeight = sortedBlocks[i].transform.position.y;
            sortedBlocks[i].transform.position = new Vector3(transform.position.x, localHeight + previousBlock.transform.position.y , previousBlock.transform.position.z + previousBlock.blockLength);
        }


        //Todo: mover esto a starter script
        TrackSign[] signs = FindObjectsOfType<TrackSign>();
        foreach(TrackSign sign in signs)
        {
            sign.GetComponent<TrackSign>().GetComponent<TextMesh>().text = id.ToString().PadLeft(2,'0');
        }
    }

    public void Win()
    {
        countdown.Reset();
        FindObjectOfType<PanelController>().ActivateWinPanel(scoreKeeper);
        FindObjectOfType<GameManager>().EndGame();
        AudioManager.Instance.PlayWinMusic();
    }


    public void Lose()
    {
        countdown.Reset();
        DestroyBlocks();
        FindObjectOfType<PanelController>().ActivateLosePanel();
        FindObjectOfType<GameManager>().EndGame();
        AudioManager.Instance.PlayLoseMusic();
        scoreKeeper.SoftReset();
    }

    // Update is called once per frame
    void Update()
    {
        //skybox
        skyBoxTimer += Time.deltaTime;
        float fraction = skyBoxTimer  / 40f;
        RenderSettings.skybox.Lerp(SkyboxMatStart, SkyboxMatEnd, fraction);
    }

    private void DestroyBlocks()
    {
        foreach(Block block in sortedBlocks)
        {
            Destroy(block,3f);
        }
    }



}
