﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Countdown : MonoBehaviour
{

    public bool Active;

    private Text textComp;

    private float timer;

    public float realTime { get; private set; }
    public float lastScore { get; private set; }


    // Start is called before the first frame update
    void Start()
    {
        realTime = -30f;
        textComp = GetComponent<Text>();
        RefreshText();
    }



    // Update is called once per frame
    void Update()
    {
        if (!Active)
            return;

        realTime += Time.deltaTime;
        timer += Time.deltaTime;
        if(timer > 0.1f)
        {
            timer -= 0.1f;
            RefreshText();
        }

    }


    public void  Pause()
    {
        Active = false;
    }


    public void StartCountdown()
    {
        Reset();
        Active = true;
    }

    public void Reset()
    {
        Pause();
        lastScore = realTime;
        realTime = -30;
        timer = 0;
        RefreshText();
        
    }



    private void RefreshText()
    {
        textComp.text = Math.Round(realTime, 1).ToString("F1");
    }
 

   
}
