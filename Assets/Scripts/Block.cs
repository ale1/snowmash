﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Block : MonoBehaviour
{
    [SerializeField]
    public int Order = -1;

    public List<Skier> skiers;

    public bool isColliding = false;

    public bool Activated = false;

    [SerializeField]
    public float blockLength;

    public float[] FiveLaneAnchors { get; private set; }  
    public float[] FourLaneAnchors { get; private set; } 


    public void Setup()
    {
        MeshRenderer mesh = GetComponent<MeshRenderer>();
        List<Floor> floors = GetComponentsInChildren<Floor>().OrderBy(e=> e.transform.position.z).ToList();
        if (floors.Count == 0)
            Debug.LogError("hmmm");


        //Z calcs
        foreach (Floor floor in floors)
        {
            blockLength += floor.length; 
        }


        // X calcs
        Floor sampleFloor = GetComponentInChildren<Floor>(); //assume que todos los floors son iguales de ancho.
        float fiveLaneWidth = sampleFloor.width / 5f;
        FiveLaneAnchors = new float[5]  { -2f * fiveLaneWidth, -fiveLaneWidth, 0, fiveLaneWidth, 2f * fiveLaneWidth };
        FourLaneAnchors = new float[4]  { -1.5f * fiveLaneWidth,  -0.5f * fiveLaneWidth, 0.5f * fiveLaneWidth,1.5f * fiveLaneWidth };

    }



    private void Awake()
    {
        Activated = false;

       
    }


    // Start is called before the first frame update
    void Start()
    {
     
    }

    // Update is called once per frame
    void Update()
    {
      
        
    }


    public void ActivateBlock()
    {
        if (Activated) return;
        Activated = true;

        //TODO: destruccion mas inteligente.
        Destroy(this.gameObject, 15f); // los bloques se destruyen 10 segundos despues se ser activados, ya que el snowball en teoria ya esta lejos cuando se acaba el timer.
    }

}
