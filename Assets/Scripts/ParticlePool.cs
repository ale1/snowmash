﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ParticlePool : MonoBehaviour
{
    //TODO: cargar por resources.
    //TODO: sacar monobehaviour.


    public ParticleSystem ParticleObstacleHit;
    private Queue<ParticleSystem> ObstacleHits = new Queue<ParticleSystem>();

    public ParticleSystem ParticleSkierHit;
    private Queue<ParticleSystem> SkierHits = new Queue<ParticleSystem>();


    Dictionary<string,Queue<ParticleSystem>> ParticleDictionary = new Dictionary<string, Queue<ParticleSystem>>();


    private void Awake()
    {

        for(int i= 0; i < 5; i++)
        {
            ParticleSystem particle = GameObject.Instantiate(ParticleObstacleHit, new Vector3(0, 0, 0), new Quaternion(),this.transform) as ParticleSystem;
            particle.gameObject.SetActive(false);
            ObstacleHits.Enqueue(particle);

        }
        ParticleDictionary.Add("ObstacleHit",ObstacleHits);

        for (int i = 0; i < 20; i++)
        {
            ParticleSystem particle = GameObject.Instantiate(ParticleSkierHit, new Vector3(0, 0, 0), new Quaternion(), this.transform) as ParticleSystem;
            particle.gameObject.SetActive(false);
            SkierHits.Enqueue(particle);

        }
        ParticleDictionary.Add("SkierHit", SkierHits);
    }


    public ParticleSystem RequestParticle(string _name, Vector3 _position)
    {
        Queue<ParticleSystem> queue = ParticleDictionary[_name];

        ParticleSystem particle = queue.Dequeue(); //remove from the front
        queue.Enqueue(particle); //put in the back.
        particle.transform.position = _position;
        particle.gameObject.SetActive(true);
        return particle;   
    }

    public ParticleSystem RequestParticle(string _name, Transform _transform)
    {

        Queue<ParticleSystem> queue = ParticleDictionary[_name];

        if (queue == null)
            Debug.LogError($"particle dictionary {_name} not found");

        ParticleSystem particle = queue.Dequeue(); //remove from the front
        queue.Enqueue(particle); //put in the back.
        particle.transform.position = _transform.position;
        StartCoroutine("PlayParticle", particle);
        return particle;
    }


    public IEnumerator PlayParticle(ParticleSystem particle)
    {
        particle.gameObject.SetActive(true);
        particle.Play();
        yield return new WaitForSeconds(2f);
        particle.Stop();
        particle.Clear();
        particle.gameObject.SetActive(false);
    }


}
