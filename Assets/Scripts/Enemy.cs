﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour
{

    protected Level currentLevel;

    //particles
    protected ParticlePool particlePool;


    //flying
    protected bool flying;
    protected float speed = 0;
    protected float flyingSpeed
    {
        get
        {
            return speed * 10;
        }
    }


    protected Route route;

    public bool Smart = true;
    public bool Acrobatic = true;

    public abstract void Fly(Route _route);

    public abstract void Kill();

    protected abstract void ApplyPirueta();

    protected void ApplyRoute()
    {

        if (route == null)
            Debug.LogError("why is" + this.gameObject.name + "skier flying but has no route?");

        if (route.Empty)
            return;

        float routeSpeed = flying ? flyingSpeed : speed;
        float step = routeSpeed * Time.deltaTime * 1.1f; //1.1 magic number por ahora. deberia ser pyth: sqrtRoot( zstep^2 + lanewidth^2) / zstep

        Vector3 waypoint = route.Next();
        transform.position = Vector3.MoveTowards(this.transform.position, waypoint, step);

        if (Acrobatic) // 
        {
            ApplyPirueta();
        }


        if (step > Vector3.Distance(transform.position, waypoint))
        {
            route.Clean();  // ya llegamos al waypoint, lo podemos quitar de nuestro route.
        }


        if (route.Empty)
        {
            flying = false;

            int layerMask = 1 << 8;  //solamente layer 8 (floor);
            RaycastHit hit;
            Vector3 rayDirection = Vector3.down;

            if (Physics.Raycast(this.transform.position, rayDirection, out hit, 10f, layerMask))
            {
                Floor currentFloor = hit.transform.gameObject.GetComponent<Floor>();
                if (currentFloor != null)
                    this.transform.position = new Vector3(transform.position.x, hit.transform.position.y, transform.position.z); //sigue en su direccion normal
                if (Smart)
                {
                    this.transform.rotation = Quaternion.identity;  //todo smooth rotate

                }

                //TODO: instanciar particulas de terminar salto.

            }
        }
    }
}
