﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Constants.GameVars;

public class Snowboarder : Enemy
{

    public ParticleSystem enemyDieVFX;

    private Snowball snowball;

    public float targetSpeed = 50f;
    public int DetectionRange = 150;
    private int frameCounter;


    private bool sleep = true;
    private bool isColliding;

    public TrailRenderer frontTrail;
    public TrailRenderer backTrail;


    private void Awake()
    {
        frameCounter = Random.Range(2, 6);  //empieza con valor random para evitar que los refresh de los esquiadores no se activen al mismo tiempo
    }


    // Start is called before the first frame update
    void Start()
    {
        particlePool = FindObjectOfType<ParticlePool>();
        snowball = FindObjectOfType<Snowball>();
        currentLevel = GetComponentInParent<Level>();

    }

    // Update is called once per frame
    void Update()
    {
        if (snowball == null)
            return;


        if (sleep)
        {
            frameCounter++;

            if (frameCounter > 6)
            {
                DetectSnowball(); //should I wake up?
                frameCounter = 0;
            }
            
        }

        if (sleep) return;  //salir de update si estoy dormido

        float adjustedSpeed = targetSpeed + (snowball.speed * 0.15f); //se ajusta un poco a la velocidad del jugador
        speed = Mathf.Lerp(speed, targetSpeed, Time.deltaTime);

        if (!flying)
        {
            this.transform.position += transform.forward * speed * Time.deltaTime;
        }
        else
        {
            ApplyRoute();
        }

    }


    private void DetectSnowball()
    {


        int adjustedRange = DetectionRange + (int)(snowball.speed * 0.1f);
        if (this.transform.position.z - snowball.transform.position.z < adjustedRange)
        {
            sleep = false;
        }
           
    }

    private void OnTriggerEnter(Collider other)
    {
        if (sleep) return;
        if (isColliding) return;
        isColliding = true;

        Sphere snowballHit = other.GetComponent<Sphere>();
        Enemy enemyHit = other.GetComponent<Enemy>();


        if( enemyHit != null)
        {
            //snowboarder se choco contra un enemigo, no hacer nada y continuar.
            isColliding = false;
        }
        else if (snowballHit != null)
        {
            ParticleSystem particle = GameObject.Instantiate(enemyDieVFX, (this.transform.position + snowball.transform.position) / 2, this.transform.rotation, snowball.transform) as ParticleSystem;
            Destroy(particle.gameObject, 1.5f);
            snowball.GoodHit(SCORE_PER_SNOWBOARDER);
            currentLevel.scoreKeeper.AddScore<Snowboarder>(); //TODO: mover esto a enemy class
            AudioManager.Instance.PlaySkierHit();
            Kill();
        }
        else
        {
            isColliding = false;
        }

        
    }

    public override void Fly(Route _route)
    {
        route = _route;
        flying = true;
        
    }

    protected override void ApplyPirueta()
    {
        #if UNITY_EDITOR
        if (route == null)
            Debug.LogError("Uh OH");
        #endif

        float progress = route.Progress;

        float RotY = Mathf.Lerp(0f, 360f, progress);
        this.transform.rotation = Quaternion.Euler(new Vector3(this.transform.rotation.y, RotY, this.transform.rotation.z)); //helicoptero
        //Debug.Log($"{route.spentWaypoints.Count} / {route.spentWaypoints.Count + route.Waypoints.Count} = {route.Progress}");    
        if (progress >= 1)
        {
            Acrobatic = false;
            backTrail.enabled = false;
        }

    }



    public override void Kill()
    {
        //TODO: animacion de caerse.
        //TODO: particulas de muerte.
        Destroy(this.gameObject);

    }

    public void KillSilently()
    {
        Destroy(this.gameObject);
    }
}
