﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Star : MonoBehaviour
{
    public bool filled { get; private set; }

    public ParticleSystem UIsparkle;

    public Sprite goldStar;
    public Sprite blackStar;

    private void Awake()
    {
        Empty(); 
    }


    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void Fill()
    {
        GetComponent<Image>().sprite = goldStar;
       //todo: Start Sparkle

       if(filled != true)
       {
            if(UIsparkle != null)
                Instantiate(UIsparkle, this.transform);
       }
        filled = true;


        
    }

    public void Empty()
    {
       GetComponent<Image>().sprite = blackStar;
       filled = false;
    }


}
