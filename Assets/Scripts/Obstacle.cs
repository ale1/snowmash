﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{

    private Route route;
    private bool flying;

    public SpriteRenderer shadow;  //todo: aceptar multiples shadows
    public SpriteRenderer sadFace;


    public ParticleSystem HitVFX;
    public ParticleSystem flyingVFX;

    private ParticlePool particlePool;

    private bool isColliding;

    // Start is called before the first frame update
    void Start()
    {
        particlePool = FindObjectOfType<ParticlePool>();
        if(sadFace)
            sadFace.gameObject.SetActive(false);

    #if UNITY_EDITOR
        if (GetComponent<BoxCollider>() == null)
            Debug.LogError(this.gameObject.name + ": te olvidaste de darle un box collider a este obstaculo");

        if (GetComponent<BoxCollider>().isTrigger == false)
            Debug.LogError(this.gameObject.name + ":  Tenes un obstacle con el collider trigger apagado. NO BUENO.");
    #endif        
    }

    // Update is called once per frame
    void Update()
    {
        if(flying)
        {
            ApplyRoute();
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (isColliding) return;   //logica para que no se dispare 2 veces, ya que sphere collider vs boxCollider tiene 2 contact points.
        isColliding = true;

        Sphere snowballHit = other.GetComponent<Sphere>();
        Enemy enemy = other.GetComponent<Enemy>();


        if (enemy != null)
        {
            //ParticleSystem particle = GameObject.Instantiate(vfx, this.transform.rotation, enemy.transform) as ParticleSystem;
           // Destroy(particle, 2f);
            enemy.Kill();
            isColliding = false;
        }
        else if (snowballHit != null)
        {

            Snowball snowball = snowballHit.snowball;
            if(HitVFX != null)
            {
                ParticleSystem particle = GameObject.Instantiate(HitVFX, (this.transform.position + snowball.transform.position) / 2, this.transform.rotation, snowball.transform) as ParticleSystem;
                Destroy(particle, 1.5f);
            }

            AudioManager.Instance.PlayTreeHit();
            snowball.BadHit();
            Fly();
            Destroy(gameObject,2f);

        }
    }


    public void Fly()
    {
        if (sadFace != null)
        {
           // sadFace.transform.LookAt(FindObjectOfType<GameCam>().transform);
           // sadFace.gameObject.SetActive(true);
        }

        //apagar shadow
        if(shadow != null)
            shadow.enabled = false;
           

        if (GetComponentInChildren<Bezier>() == null)
            return;

        //TODO optimizar para genrar la curva onlevelload
        route = GetComponentInChildren<Bezier>().GenerateCheapRoute(this.transform.position, this.transform.position + new Vector3(0, 200f, 200f));
        flying = true;

    }

    protected void ApplyRoute()
    {

        if (route == null)
            Debug.LogError("why is" + this.gameObject.name + "skier flying but has no route?");

        if (route.Empty)
            return;

        float routeSpeed = 320f;
        float step = routeSpeed * Time.deltaTime;

        Vector3 waypoint = route.Next();
        transform.position = Vector3.MoveTowards(this.transform.position, waypoint, step);

        if (step > Vector3.Distance(transform.position, waypoint))
        {

            route.Clean();  // ya llegamos al waypoint, lo podemos quitar de nuestro route.
            if (flyingVFX != null)
            {
                ParticleSystem particle = GameObject.Instantiate(flyingVFX, this.transform.position, this.transform.rotation, this.transform.parent) as ParticleSystem;
                Destroy(particle, 1f);
            }
        }

        if (route.Empty)
        {
            flying = false;
            //TODO: instanciar particulas.

        }
    }
}
