﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Constants.GameVars;

public class Skier : Enemy
{
    public ParticleSystem SkierDieVFX;

    private Snowball snowball;

    public float targetSpeed = 50f;
    public int DetectionRange = 150;
    private int frameCounter;


    private bool sleep = true;
    private bool isColliding;



    private void Awake()
    {
        frameCounter = Random.Range(1, 5);  //empieza con valor random para que todos los refresh de los esquiadores no se activen al mismo tiempo
    }


    // Start is called before the first frame update
    void Start()
    {
        particlePool = FindObjectOfType<ParticlePool>();
        snowball = FindObjectOfType<Snowball>();
        currentLevel = GetComponentInParent<Level>();
	
    }

    // Update is called once per frame
    void Update()
    {

        if (sleep)
        {
            frameCounter++;

            if (frameCounter > 6)
            {
                DetectSnowball(); //should I wake up?
                frameCounter = 0;
            }
            
        }

        if (sleep) return;  //salir de update aqui si estoy dormido

        float adjustedSpeed = targetSpeed + (snowball.speed * 0.15f); //se ajusta un poco a la velocidad del jugador
        speed = Mathf.Lerp(speed, adjustedSpeed, Time.deltaTime);

        if (!flying)
        {
            this.transform.position += transform.forward * speed * Time.deltaTime;
        }
        else
        {
            ApplyRoute();
        }

    }


    private void DetectSnowball()
    {
        int adjustedRange = DetectionRange + (int)(snowball.speed * 0.1f);
        if (this.transform.position.z - snowball.transform.position.z < adjustedRange)
        {
            sleep = false;
        }    
    }

    private void OnTriggerEnter(Collider other)
    {
        if (sleep) return;
        if (isColliding) return;
        isColliding = true;

        Sphere snowballHit = other.GetComponent<Sphere>();
        Enemy enemyHit = other.GetComponent<Enemy>();


        if(enemyHit != null && enemyHit is Skier)
        {
            //collider con otro esquiador:  Se mueren ambos.  si el otro era snowboarder, solo se muere el esquiador.
            //ParticleSystem particle = GameObject.Instantiate(SkierDieVFX, this.transform.position, this.transform.rotation, skierHit.transform) as ParticleSystem;
            //Destroy(particle, 2f);
            Kill();
        }
        else if (snowballHit != null)
        {
            ParticleSystem particle = GameObject.Instantiate(SkierDieVFX,  (this.transform.position + snowball.transform.position) / 2, this.transform.rotation, snowball.transform) as ParticleSystem;
            Destroy(particle.gameObject, 1.5f);
            snowball.GoodHit(SCORE_PER_SKIER);
            currentLevel.scoreKeeper.AddScore<Skier>();
            AudioManager.Instance.PlaySkierHit();
            Kill();
        }
        else
        {
            isColliding = false;
        }
    }

    public override void Fly(Route _route)
    {
        route = _route;
        flying = true;
    }


    protected override void ApplyPirueta()
    {
        #if UNITY_EDITOR
        if (route == null)
            Debug.LogError("Uh OH");
        #endif

        float progress = route.Progress;

        float RotX = Mathf.Lerp(0f, 360f, progress);
        this.transform.rotation = Quaternion.Euler(new Vector3(RotX, this.transform.rotation.y, this.transform.rotation.z)); //forward loop
        //Debug.Log($"{route.spentWaypoints.Count} / {route.spentWaypoints.Count + route.Waypoints.Count} = {route.Progress}");    
        if (progress >= 1)
        {
            Acrobatic = false;
        }

    }


    public override void Kill()
    {
        //Debug.Log(this.name + " hit obstacle");
        //TODO: animacion de caerse.
        Destroy(this.gameObject);

    }

    public void KillSilently()
    {
        Destroy(this.gameObject);
    }
}
