﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;


namespace Constants {

    public static class GameVars
    {
        //Score to Stage Conversion
        public const int MAXCOMBO = 200;  //se usa para llenar barras de UI.
        public const int STAGE_5_COMBO = 75;
        public const int STAGE_4_COMBO = 50;
        public const int STAGE_3_COMBO = 25;
        public const int STAGE_2_COMBO = 10;
        public const int STAGE_1_COMBO = 0;


        //Score per enemy
        public const int SCORE_PER_SKIER = 1;
        public const int SCORE_PER_SNOWBOARDER = 2;
        public const int SCORE_PER_SNOWBIKER = 5;
        public const int SCORE_PER_SECOND = -1;
    }




}


