﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{

    private bool locked;

    public PanelController panelController;

    public int SelectedLevel = 1;
    private List<Level> Levels;

    //cams
    public GameCam gameCam { get; private set; }


    public GameObject playerBallPrefab;
    private Snowball snowBall;
    private Level activeLevel;


    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        Levels = new List<Level>();
    }


    // Start is called before the first frame update
    void Start()
    {
        SaveData saveData = FindObjectOfType<SaveManager>().savedata;
        Levels = Resources.LoadAll<Level>("Levels").ToList();
        for(int i = 0; i < Levels.Count; i++)
        {
            int id = i + 1;
            Levels[i].id = id;

            if (saveData.scoreKeepers.ContainsKey(id))
                Levels[i].scoreKeeper = saveData.scoreKeepers[id];    
        }

        gameCam = FindObjectOfType<GameCam>();
        AudioManager.Instance.PlayIntroMusic();
    }



    public void StartGame()
    {
        if (locked)
            return;

        locked = true;

        Level targetLevel = Levels.FirstOrDefault(x => x.id == SelectedLevel);

        //si no existe el nivel, te resetea el progress counter y emepzas devuelta del nivel 1
        if(targetLevel == null)
        {
           Debug.Log($"EL NIVEL {SelectedLevel} NO EXISTE!. Reseteando");
           SelectedLevel = 1;
           targetLevel = Levels.First(x=> x.id == SelectedLevel);
        }
        else
        {
            Debug.Log($"STARTING LEVEL {targetLevel.id}");
        }


        activeLevel = Instantiate(targetLevel);

        GameObject playerBallGO = Instantiate(playerBallPrefab);
        snowBall = playerBallGO.GetComponent<Snowball>();
        snowBall.SetupBall(activeLevel);
        gameCam.Snowball = snowBall; //avisarle al gameCam
        FindObjectOfType<InputManager>().snowball = snowBall;  //avisarle al inputManager
        FindObjectOfType<InputManager>().gameCam = gameCam;

        //UI
        panelController.ActivatePlayPanel();

        //music
        AudioManager.Instance.PlayGameMusic();
    }


    public void EndGame()
    {
        FindObjectOfType<SaveManager>().Save();
        if (activeLevel != null)
            Destroy(activeLevel.gameObject);
        if (snowBall != null)
            Destroy(snowBall.gameObject);
        locked = false;
       

    }

    public void QuitLevel()
    {
        EndGame();
        panelController.ActivateHome();
        AudioManager.Instance.PlayMenuMusic();
        //Debug.Log(SaveManager.Instance.savedata.scoreKeepers[1]);
    }

    // Update is called once per frame
    void Update()
    {
 
    }

    public void ResetProgress()
    {
        foreach (var level in Levels)
        {
            level.scoreKeeper = null;
        }
        FindObjectOfType<SaveManager>().Save();
    }

    
    public void RestartLevel()
    {
        EndGame();
        StartGame();
    }

    public void NextLevel()
    {
        EndGame();
        SelectedLevel++;
        StartGame();
    }
    

    public void QuitGame()
    {
        Application.Quit();
    }


}
