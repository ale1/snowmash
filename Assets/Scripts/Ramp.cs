﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ramp : MonoBehaviour
{
    private Bezier bezier;

    // Start is called before the first frame update
    void Start()
    {
         bezier = GetComponentInChildren<Bezier>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void OnTriggerEnter(Collider other)
    {

        Snowball snowball = other.GetComponentInParent<Snowball>();
        Enemy enemy = other.GetComponent<Enemy>();

        if (snowball != null)
        {
            Route flightRoute = bezier.GenerateManualRoute(snowball.transform);
            snowball.Fly(flightRoute);

        }
        else if(enemy != null)
        {
            Route flightRoute = bezier.GenerateManualRoute(enemy.transform,false);
            enemy.Fly(flightRoute);
        }
   }




}
