﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InputManager : MonoBehaviour
{

    [HideInInspector]
    public Snowball snowball;
    [HideInInspector]
    public GameCam gameCam;

    string lastMovement;
    private float wait = 0;
    private float swipeWait = 0.2f; //todo: ir a constants.
    private float tapWait = 0.02f;

    public Action<string> OnMovementAdded;


    private Vector2 fingerDown;
    private Vector2 fingerUp;


    public InputStyle inputStyle = InputStyle.Taps; 
    public enum InputStyle
    {
        FullSwipes,
        LazySwipes,
        Taps
    }
   

    private float swipe_threshold;


    private void Awake()
    {
        swipe_threshold = 40f;
    }


    // Update is called once per frame
    void Update()
    {

        if (wait > 0)
            wait -= Time.deltaTime;

        if (!snowball)
            return;

        //Toggle Debug
        if (Input.GetKeyDown(KeyCode.A))
        {
            lastMovement = "MoveLeft";
            OnMovementAdded?.Invoke(lastMovement);
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            lastMovement = "MoveRight";
            OnMovementAdded?.Invoke(lastMovement);
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            lastMovement = "Dash";
            OnMovementAdded?.Invoke(lastMovement);
        }

        #region taps
        if (inputStyle == InputStyle.Taps)
        {
            foreach(Touch touch in Input.touches)
            {
                if(touch.phase == TouchPhase.Began)
                {
                    Vector3 ballPos = gameCam.GetComponent<Camera>().WorldToScreenPoint(snowball.transform.position); 
                   
                    if(touch.position.x > ballPos.x)
                    {
                        OnTapRight();
                    }

                    if(touch.position.x < ballPos.x)
                    {
                        OnTapLeft();
                    }

                }
            }
        }
        #endregion

       
       


        #region Swipes
        if(inputStyle == InputStyle.FullSwipes || inputStyle == InputStyle.LazySwipes)
        {
            foreach (Touch touch in Input.touches)
            {
                if (touch.phase == TouchPhase.Began)
                {
                    fingerUp = touch.position;
                    fingerDown = touch.position;
                }

                //Detects Swipe while finger is still moving
                if (touch.phase == TouchPhase.Moved)
                {
                    if (inputStyle == InputStyle.LazySwipes)
                    {
                        fingerDown = touch.position;
                        checkSwipe();
                    }
                }

                //Detects swipe after finger is released
                if (touch.phase == TouchPhase.Ended)
                {
                    fingerDown = touch.position;
                    checkSwipe();
                }
            }
        }
        
        #endregion
    }

    void checkSwipe()
    {
        //Check if Vertical swipe
        if (verticalMove() > swipe_threshold && verticalMove() > horizontalValMove())
        {
            //Debug.Log("Vertical");
            if (fingerDown.y - fingerUp.y > 0)//up swipe
            {
                OnSwipeUp();
            }
            else if (fingerDown.y - fingerUp.y < 0)//Down swipe
            {
                OnSwipeDown();
            }
            fingerUp = fingerDown;
        }

        //Check if Horizontal swipe
        else if (horizontalValMove() > swipe_threshold && horizontalValMove() > verticalMove())
        {
            //Debug.Log("Horizontal");
            if (fingerDown.x - fingerUp.x > 0)//Right swipe
            {
                OnSwipeRight();
            }
            else if (fingerDown.x - fingerUp.x < 0)//Left swipe
            {
                OnSwipeLeft();
            }
            fingerUp = fingerDown;
        }

        //No Movement at-all
        else
        {
            //Debug.Log("No Swipe!");
        }
    }

    float verticalMove()
    {
        return Mathf.Abs(fingerDown.y - fingerUp.y);
    }

    float horizontalValMove()
    {
        return Mathf.Abs(fingerDown.x - fingerUp.x);
    }

    //////////////////////////////////CALLBACK FUNCTIONS/////////////////////////////
    private void OnSwipeUp()
    {
        Debug.Log("Swiped up");
    }

    private void OnSwipeDown()
    {
        Debug.Log("Swiped Down");
    }

   private  void OnSwipeLeft()
    {

        if(wait > 0 && lastMovement == "MoveLeft")
        {
            return;
        }
        else
        {
            lastMovement = "MoveLeft";
            OnMovementAdded?.Invoke(lastMovement);
            wait = swipeWait;
        }

      
    }

    private void OnSwipeRight()
    {
        if (wait > 0 && lastMovement == "MoveRight")
        {
            return;
        }
        else
        {
            lastMovement = "MoveRight";
            OnMovementAdded?.Invoke(lastMovement);
            wait = swipeWait;
        }
    }

    private void OnTapLeft()
    {
        lastMovement = "MoveLeft";
        OnMovementAdded?.Invoke(lastMovement);
        wait = tapWait;

    }
    private void OnTapRight()
    {
        lastMovement = "MoveRight";
        OnMovementAdded?.Invoke(lastMovement);
        wait = tapWait;
    }
}
