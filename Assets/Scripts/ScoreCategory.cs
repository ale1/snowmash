﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreCategory
{
    public string Description;
    public int PointsPerItem;

    private int  itemCount;
    public int ItemCount {
        get { return itemCount; }
        set {
            itemCount = value;
            if (itemCount > BestItemCount)
                BestItemCount = itemCount;
        }
    }
    public int BestItemCount;

    public float Threshold;
    public int ThresholdCount {
        get { return (int)(MaxCount * Threshold); }
    }
    public float ItemCountNormalized
    {
        get {   return (float)ItemCount / (float)MaxCount;}
}
    public int CategoryScore
    {
        get { return PointsPerItem * ItemCount; }
    }

    public int MaxCount = 0;
    public bool StarFilled
    {
        get { return BestItemCount > ThresholdCount; }
    }


    //parameterless constructor for serialization only
    private ScoreCategory()
    {

    }

    public ScoreCategory(string _Description, int _PointsPerItem, float _Threshold)
    {
        itemCount = 0;
        Description = _Description;
        PointsPerItem = _PointsPerItem;
        BestItemCount = 0;
        Threshold = _Threshold;
    }

    public void HardReset()
    {
        ItemCount = 0;
    }

    public void SoftReset()
    {
        ItemCount = 0;
    }





    



}
