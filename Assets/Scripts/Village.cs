﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Village : MonoBehaviour
{
    private GameManager gameManager;

    public Animator Avalanche;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private bool isColliding = false;
    private void OnTriggerEnter(Collider other)
    {

        if (isColliding) return;
        isColliding = true;

        Snowball snowball = other.gameObject.GetComponentInParent<Snowball>();

        if (snowball != null)
        {
            FindObjectOfType<CameraShake>().ShakeCamera(0.9f,0.12f);
            Avalanche.Play("start");
            snowball.gameObject.SetActive(false);
            StartCoroutine(WinSequence());
        }
       
    }

    IEnumerator WinSequence()
    {
        yield return new WaitForSeconds(3);
        GetComponentInParent<Level>().Win();
    }


}
