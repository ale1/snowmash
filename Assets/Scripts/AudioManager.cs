﻿using System;
using UnityEngine;
using UnityEngine.Audio;
using System.Linq;
using System.Collections.Generic;


// todo: usar class ClipData dentro de Soundbank

public class ClipData
{
    string name;
    AudioClip clip;
    string path;
    double duration;
    double startTime;


    public ClipData(string _path)
    {
        //todo: diccionario.
        path = _path;
        name = "todo";
        clip = Resources.Load<AudioClip>(name);
        duration = (double)clip.samples / clip.frequency;
        startTime = AudioSettings.dspTime + 0.2;
    }
}


public class AudioManager : MonoBehaviour
{

    public bool mute;

    public AudioSource[] MusicPlayers;
    public AudioSource[] SFXPlayers;

    public static AudioManager Instance;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this.gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(this.gameObject);
    }


    private void Start()
    {
        mute = SaveManager.Instance.savedata.mute;
        AudioListener.pause = mute;

    }

    public void ToggleMute()
    {
        SaveManager saveManager = SaveManager.Instance;
        mute ^= true;
        saveManager.savedata.mute = mute;
        saveManager.Save();

        AudioListener.pause = mute;

    }


    public AudioClip Clip(string name)
    {
       AudioClip clip =   Resources.Load<AudioClip>(name);
        if (clip != null)
            return clip;
        else
        {
            //Debug.LogError("clip not found:" + name);
            return null;
        }
    }

    private void PlayOneTimeMusic(AudioClip _clip)
    {
        AudioSource musicPlayer = MusicPlayers[0];
        musicPlayer.Stop();
        musicPlayer.loop = false;
        AudioClip oneTimeClip = _clip;
        musicPlayer.clip = oneTimeClip;
        musicPlayer.PlayScheduled(AudioSettings.dspTime);

        AudioSource musicPlayer2 = MusicPlayers[1];
        musicPlayer2.Stop();
        musicPlayer2.loop = true;
        musicPlayer2.clip = Clip(SoundBank.MenuMusic);
        if (oneTimeClip != null)
            musicPlayer2.PlayScheduled(AudioSettings.dspTime + (double)oneTimeClip.samples / oneTimeClip.frequency);
        else
            musicPlayer2.PlayScheduled(AudioSettings.dspTime);
    }


    public void PlayWinMusic()
    {
        AudioClip winClip = Clip(SoundBank.WinMusic);
        PlayOneTimeMusic(winClip);
    }

    public void PlayLoseMusic()
    {
        AudioClip loseClip = Clip(SoundBank.LoseMusic);
        PlayOneTimeMusic(loseClip);
    }

    public void PlayIntroMusic()
    {
        AudioClip introClip = Clip(SoundBank.IntroMusic);
        PlayOneTimeMusic(introClip);
    }

    //todo refactor playmenumusic and playgamemusic
    public void PlayMenuMusic()
    {
        foreach (AudioSource source in MusicPlayers)
        {
            source.Stop();
        }
        AudioSource musicPlayer = MusicPlayers[0];
        musicPlayer.loop = true;
        musicPlayer.clip = Clip(SoundBank.MenuMusic);
        musicPlayer.Play();
    }

    public void PlayGameMusic()
    {
        foreach (AudioSource source in MusicPlayers)
        {
            source.Stop();
        }

        AudioSource musicPlayer = MusicPlayers[0];
        musicPlayer.loop = true;
        musicPlayer.clip = Clip(SoundBank.GameMusic);
        musicPlayer.Play();
    }
    
    public void PlaySkierHit()
    {
        if (mute)
            return;

        foreach (AudioSource AS in SFXPlayers)
        {
            if (!AS.isPlaying)
            {
                AudioClip clip = Clip(SoundBank.SkierCollision);
                AS.PlayOneShot(clip, 0.60f);
                break;
            }
        }      
        
    }

    public void PlayTreeHit()
    {
        if (mute)
            return;

        foreach (AudioSource AS in SFXPlayers)
        {
            if (!AS.isPlaying)
            {
                AudioClip clip = Clip(SoundBank.TreeCollision);
                AS.PlayOneShot(clip, 1f);
                break;
            }
        }
    }



    public static class SoundBank // pseudo-enum hack que permite strings.   TODO:  hacerle un accesor, y poner este como private. 
    {
  
        //Music
        public const string GameMusic = "Audio/GameMusic";
        public const string MenuMusic = "Audio/MenuMusic";
        public const string IntroMusic = "Audio/IntroMusic";
        public const string WinMusic = "Audio/WinMusic";
        public const string LoseMusic = "Audio/LoseMusic";
        public const string FeverMusic = "EJEMPLO/Audio/Music/FeverMusic";

        //Gameplay
        public const string SkierCollision = "Audio/SkierHit";
        public const string TreeCollision = "Audio/Thump";
        public const string RockCollision = "EJEMPLO/Audio/RockCollision";
        public const string FenceCollision = "EJEMPLO/Audio/FenceCollision";

        //UI
        public const string ComicPageFlip = "EJEMPLO/Audio/UI/ComicPageFlip";

    }

}



