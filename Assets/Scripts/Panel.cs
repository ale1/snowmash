﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Panel : MonoBehaviour
{
    protected PanelController panelController;

    [SerializeField]
        private Vector3 DefaultPos;

    private Animator animator;

    private void Awake()
    {
        DefaultPos = this.transform.localPosition;
        animator = GetComponent<Animator>();
        panelController = GetComponentInParent<PanelController>();
    }


    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Show()
    {

        this.transform.localPosition = new Vector3(0, DefaultPos.y, 0);
        if (animator.enabled)
            animator.Play("Show");

        ShowInternal();

    }

    public void Hide()
    {
        this.transform.localPosition = DefaultPos;
       // Debug.Log("hid " + this.name);
        if (animator.enabled)
            animator.Play("Hide");

        HideInternal();


    }


    private float fadeTimer = 0.4f;
    private IEnumerator FadeIn(Image _image)
    {
        float timer = 0f;
        Color c = _image.color;
        while (timer < fadeTimer)
        {
            timer += Time.deltaTime;
            c.a = Mathf.Clamp01(timer / fadeTimer);
            _image.color = c;

            yield return new WaitForEndOfFrame();
        }
        yield return null;
    }

    private IEnumerator FadeOut(Image _image)
    {
        float timer = 0f;
        Color c = _image.color;
        while (timer < fadeTimer)
        {
            timer += Time.deltaTime;
            c.a = 1f - Mathf.Clamp01(timer / fadeTimer);
            _image.color = c;

            yield return new WaitForEndOfFrame();
        }

        yield return null;
    }


    protected abstract void HideInternal();

    protected abstract void ShowInternal();




}
