﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Constants.GameVars;

public class ScoreKeeper
{
    public int id;  //tiene que coincidir con el id del nivel;
    public ScoreCategory Skiers;
    public ScoreCategory Snowboarders;
    public ScoreCategory SnowBikers;
    public List<ScoreCategory> ScoreCats;


    //parameterless constructor for serialization only
    private ScoreKeeper()
    {

    }

    public ScoreKeeper(int _id)
    {
        id = _id;
        Skiers = new ScoreCategory("SkiersKilled", SCORE_PER_SKIER, 0.5f) ;
        Snowboarders = new ScoreCategory("SnowboardersKilled",  SCORE_PER_SNOWBOARDER, 0.3f);
        SnowBikers = new ScoreCategory("SnowMobilesKilled", SCORE_PER_SNOWBIKER, 0.1f);
        ScoreCats = new List<ScoreCategory>() { Skiers, Snowboarders, SnowBikers };
    }

    public void AddScore<T>()
    {
      if(typeof(T) == typeof(Skier))
      {
         Skiers.ItemCount++;
      }
      else if(typeof(T) == typeof(Snowboarder))
      {
            Snowboarders.ItemCount++;
      }
      else if(typeof(T) == typeof(Snowbiker))
      {
            SnowBikers.ItemCount++;
      }
      else
      {
            Debug.Log("did not recognize score category type");
      }
    }


    public void SetMaxCount<T>(int _max)
    {
        if (typeof(T) == typeof(Skier))
        {
            Skiers.MaxCount = _max;
        }
        else if (typeof(T) == typeof(Snowboarder))
        {
            Snowboarders.MaxCount = _max;
        }
        else if (typeof(T) == typeof(Snowbiker))
        {
            SnowBikers.MaxCount = _max;
        }
        else
        {
            Debug.Log("did not recognize score category type");
        }
    }

    public void SoftReset()
    {
        foreach(ScoreCategory scoreCat in ScoreCats)
        {
            scoreCat.SoftReset();
        }
    }

    public void HardReset()
    {
        foreach (ScoreCategory scoreCat in ScoreCats)
        {
            scoreCat.HardReset();
        }
    }

   

}
