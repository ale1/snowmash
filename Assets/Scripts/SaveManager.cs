﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    public bool cleanSaveDataOnStart;


    public static SaveManager Instance { set; get; }
    public SaveData savedata;

    private void Awake()
    {
        Instance = this;

        if (cleanSaveDataOnStart)
            PlayerPrefs.DeleteKey("save");

        Load();
        DontDestroyOnLoad(this.gameObject);
    }

    public void Write()
    {
       //todo  usar writers y readers en vez de tocar savedata directamente.
    }



    public void Save()
    {
        PlayerPrefs.SetString("save", Tools.Serialize<SaveData>(savedata) );
    }

    public void Load()
    {
        if(PlayerPrefs.HasKey("save"))
        {
            savedata = Tools.Deserialize<SaveData>(PlayerPrefs.GetString("save"));
        }
        else  //new savedata as it doesnt exist
        {
            savedata = new SaveData();
            FindObjectOfType<GameManager>().ResetProgress();
            Save();
            Debug.Log("no savedata, creating new one");
        }
    }

 

}
