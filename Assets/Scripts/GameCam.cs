﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCam : MonoBehaviour
{

    private float Elasticity = 4;

    private Snowball snowball;
    public Snowball Snowball {
        get { return snowball; }
        set
        {
            Reset();
            snowball = value;
            snowball.OnSwitchFever += ActivateFeverEffect;
            snowball.OnFlying += ActivateFlyingEffect;
            snowball.OnDeath += SetDeathMode;
        }
    }


    private float effectTimer = 0.5f;

    public Vector3 Offset; //seteado por inspector
    private Vector3 offset;

    public GameObject FeverEffect;
    public GameObject FlyingEffect;



    // How long the object should shake for.
    public float shakeDuration = 0f;

    // Amplitude of the shake. A larger value shakes the camera harder.
    public float shakeAmount = 2.1f;
    public float decreaseFactor = 0.8f;

    private Vector3 originalPos; // goes back here after shaking.






    // Start is called before the first frame update
    void Start()
    {
        ActivateFeverEffect(false);
        ActivateFlyingEffect(false);
        offset = Offset;
        
    }

    // Update is called once per frame
    void Update()
    {
 
        if (Snowball != null)
        {
            offset = new Vector3(offset.x, offset.y, offset.z); 
            Vector3 targetPos = new Vector3(0, Snowball.transform.position.y, Snowball.transform.position.z) + offset;
            //float distance = Vector3.Distance(transform.position, targetPos);
            transform.position = Vector3.Lerp(transform.position, targetPos, Elasticity * Time.deltaTime);
        }

        if (effectTimer > 0)
            effectTimer -= Time.deltaTime;



        if (shakeDuration > 0)
        {
            transform.position = originalPos + Random.insideUnitSphere * shakeAmount;

            shakeDuration -= Time.deltaTime * decreaseFactor;
        }



    }

    public void Shake()
    {
        originalPos = transform.position;
        shakeDuration = 0.5f;
    }




    private void ActivateFeverEffect(bool feverSwitch)
    {
        if (feverSwitch)
            FeverEffect.SetActive(true);
        else
            FeverEffect.SetActive(false);
    }

    private void ActivateFlyingEffect(bool flyingswitch)
    {
        
        if (effectTimer > 0)
            return;  //prevenimos que se active/desactive demasiado seguido, en caso de pelota que rebota.
      
        if (snowball && snowball.hasFever) //
            return; //one cam effect a la vez

        if (flyingswitch)
            FlyingEffect.SetActive(true);
        else
            FlyingEffect.SetActive(false);


        effectTimer = 0.5f;
    }


    public void Reset()
    {
        offset = Offset;  //volver al offset original del inspector;
        Elasticity = 4;
        this.GetComponent<Camera>().farClipPlane = 420;
        ActivateFlyingEffect(false);
        ActivateFeverEffect(false);
        if(Snowball) //referencia a snowball viejo.
        {
            Snowball.OnSwitchFever -= ActivateFeverEffect;
            Snowball.OnFlying -= ActivateFlyingEffect;
        }

        StopAllCoroutines();
       
    }

    public void SetFinaleMode()
    {
        this.GetComponent<Camera>().farClipPlane = 5000;
        StartCoroutine("FinaleModeRoutine");
    }


    public void SetDeathMode()
    {
        StartCoroutine("DeathModeRoutine");
    }

    public IEnumerator FinaleModeRoutine()
    {
        while (snowball != null)
        {
            offset = Vector3.Lerp(offset, new Vector3(0f, 25f, -200f), 0.1f);
            Elasticity = Mathf.Lerp(Elasticity, 1.5f, 0.1f);
       
            yield return new WaitForSeconds(0.1f);
        }
        yield break;
    }

    public IEnumerator DeathModeRoutine()
    {
        while (snowball != null)
        {
            offset = Vector3.Lerp(offset, new Vector3(0, 25f, -30f),0.05f);
            yield return new WaitForSeconds(0.05f);
        }
        yield break;
    }





}
