﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalRamp : MonoBehaviour
{
    private bool isColliding;

    GameManager GM;

    // Start is called before the first frame update
    void Start()
    {
        GM = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void OnTriggerEnter(Collider other)
    {
        if (isColliding) return;   //logica para que no se dispare 2 veces, ya que sphere collider vs boxCollider tiene 2 contact points.
        isColliding = true;

        Snowball snowball = other.GetComponentInParent<Snowball>();

        if(snowball != null)
        {
            GM.gameCam.SetFinaleMode();
            snowball.GetComponentInChildren<SnowtrailController>().FinaleMode();
        }
    }
}
