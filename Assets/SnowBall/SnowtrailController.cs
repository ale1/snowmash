﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowtrailController : MonoBehaviour
{
    public TrailRenderer snowtrail;
    public Texture[] Textures  = new Texture[5];

    private Vector3 RotFrom = new Vector3(0.0F, 30.0F, 0.0F);
    private Vector3 RotTo = new Vector3(0.0F, -30.0F, 0.0F);
    private float frequency = 1.0F;

    private bool fading = false;
    private Texture targetTexture;

    private float timer = 0f;

    private AnimationCurve currentCurve;
    private AnimationCurve targetCurve;

    private List<Keyframe[]> keys = new List<Keyframe[]>();

    private Keyframe[] FinaleKeys = new Keyframe[3];



    private void Awake()
    {
    

        keys.Add( new[] { new Keyframe(0, 0.05f), new Keyframe(0.5f, 0.1f), new Keyframe(1f, 1f) });
        keys.Add( new[] { new Keyframe(0, 0.08f), new Keyframe(0.5f, 0.15f), new Keyframe(1f, 1f) });
        keys.Add( new[] { new Keyframe(0, 0.16f), new Keyframe(0.5f, 0.3f), new Keyframe(1f, 1f) });
        keys.Add( new[] { new Keyframe(0, 0.28f), new Keyframe(0.5f, 0.5f), new Keyframe(1f, 1f) });
        keys.Add( new[] { new Keyframe(0, 0.4f), new Keyframe(0.5f, 0.8f), new Keyframe(1f, 1f) });

        FinaleKeys = new Keyframe[3] { new Keyframe(0, 0.3f), new Keyframe(0.5f, 0.5f), new Keyframe(1f, 0f) };

        snowtrail.numCornerVertices = 24;

    }

    protected virtual void Update()
    {
        Swing();

        if (!fading)
            return;

        if(timer < 1.2)
        {

            timer += Time.deltaTime;

            //curve transition
            for (int i = 0; i < currentCurve.keys.Length; i++)
            {
                float targetTime = Mathf.Lerp(currentCurve.keys[i].time, targetCurve.keys[i].time, timer /1.2f);
                float targetValue = Mathf.Lerp(currentCurve.keys[i].value, targetCurve.keys[i].value, timer /1.2f);
                Keyframe targetFrame = new Keyframe(targetTime, targetValue);
                currentCurve.MoveKey(i, targetFrame);
            }
            snowtrail.widthCurve = currentCurve;


            //material transition

            snowtrail.material.SetFloat("_Blend", timer /1.2f);
        }
        else
        {
            fading = false;
            snowtrail.material.SetTexture("_MainTex", targetTexture);
        }


    }


    private void Swing()
    {
        Quaternion from = Quaternion.Euler(this.RotFrom);
        Quaternion to = Quaternion.Euler(this.RotTo);

        float lerp = 0.5F * (1.0F + Mathf.Sin(Mathf.PI * Time.realtimeSinceStartup * this.frequency));
        this.transform.localRotation = Quaternion.Lerp(from, to, lerp);
    }

    public void SetTrail(int _stage)
    {

        if (_stage == 0) //estas muerto
        {
            this.snowtrail.enabled = false;
            return;
        }
           

        currentCurve = snowtrail.widthCurve;
        targetCurve = new AnimationCurve();
        targetCurve.keys = keys[_stage - 1];


        //material transitions

       targetTexture = Textures[_stage - 1];

       Texture currentTexture = snowtrail.material.GetTexture("_MainTex");
       snowtrail.material.SetTexture("_Texture2", targetTexture);

        fading = true;
        timer = 0;
    }

    public void FinaleMode()
    {
        StartCoroutine("FinaleModeRoutine");
        snowtrail.time = 0.3f;
        snowtrail.widthMultiplier = 0.5f;
        snowtrail.numCapVertices = 5;
        snowtrail.widthCurve = new AnimationCurve(FinaleKeys);
    }

    public IEnumerator FinaleModeRoutine()
    {
       while (snowtrail != null )
       {
            snowtrail.time = Mathf.Lerp(snowtrail.time, 0.3f, 0.01f);
            snowtrail.widthMultiplier = Mathf.Lerp(snowtrail.widthMultiplier, 15f, 0.1f);
            yield return new WaitForSeconds(0.1f);
       }
       yield return null;
    }





}
