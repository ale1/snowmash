﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;
using static Constants.GameVars;

public class Snowball : MonoBehaviour
{
    //VFX
    public ParticleSystem StageGrowVFX;
    public ParticleSystem GoodHitVFX;
    public ParticleSystem GroundSlamVFX;
    public ParticleSystem RightFlakes;
    public ParticleSystem LeftFlakes;
    public ParticleSystem CentralFlakes;

    private SnowtrailController trailController;

    public GameObject FowRotContainer;
    private Sphere sphere;
    public GameObject FeverParticles;
    public TrailRenderer trail;
    private Bezier bezier;
    private Level CurrentLevel;

    private FloatingText floatingText;

    private ScoreKeeper scores
    {
        get { return CurrentLevel.scoreKeeper; }

    }

    [SerializeField]
    private float LeftEdge;
    [SerializeField]
    private float RightEdge;

    //combos and energy

    private int combo; //no tocar
    private int Combo
    {
        get { return combo; }
        set
        {
            combo = value;
            foreach(EnergyBar bar in bars)
            {
                bar.Refresh(value);
            }
            energyballController.Refresh(value);
        }
    }
   



    //speed
    public float speed = 0f;

    private int stage; //no tocar.
    public int Stage
    {
        get { return stage; }
        private set
        {
           
            if (stage == value)
            {
                #if UNITY_EDITOR
                Debug.LogError("trying to set new stage value same as current: " + value);
                #endif
                return;
            }

            stage = value;
            SetStageSpeed();
            trailController.SetTrail(value);
            floatingText.Activate(value, this.transform);
            sphere.GetComponent<Animator>().SetInteger("Stage", Stage);
        }
    }

    //timers
    private float gravityTimer;
    private float comboTimer;
    private float frictionTimer;
    private float feverTimer;
    private float inmunityTimer = 0;


    private float laneWidth;
    public int currentLane { get; private set; }  //TODO: hacer que empieze de 0, en vez de 1, asi coincide con indexes de anchor lanes.

    private List<Action> MovementQueue = new List<Action>();
    private Block CurrentBlock = null;
    private Floor CurrentFloor = null;
    private Route route = null;

    //Flying
    private bool Flying = false;


    //Jumping
    private bool Jumping;
    private float jumpingSpeed
    {
        get { return speed * 1.2f; }
    }

    //fever
    public bool hasFever
    {
        get { return Stage == 5; }
    }
    private float feverSpeed
    {
        get { return 320f; }
    }


    //dashing
    private bool Dashing;
    private float dashingSpeed
    {
        get { return 1000f; }
    }

    //Debug
    public bool DebugMode = false;


    //Actions
    public Action<bool> OnSwitchFever;
    public Action<bool> OnFlying;
    public Action OnDeath;


    private EnergyBar[] bars;
    private EnergyBallController energyballController;



    private void Awake()
    {
        floatingText = FindObjectOfType<FloatingText>();
        sphere = GetComponentInChildren<Sphere>();
    }

    // Start is called before the first frame update
    void Start()
    {

        trailController = GetComponentInChildren<SnowtrailController>();
        bezier = GetComponentInChildren<Bezier>();

        currentLane = 3;

        FeverParticles.gameObject.SetActive(false);

        //starting stage and energy bar
        bars = FindObjectsOfType<EnergyBar>();
        energyballController = FindObjectOfType<EnergyBallController>();
        energyballController.Restart(this);
        Stage = 2;
        foreach(EnergyBar bar in bars )
        {
            bar.Reset();
        }
        trailController.SetTrail(Stage);

    }

    // Update is called once per frame
    void Update()
    {

       // ApplyFriction();
        ApplyGravity();
        ApplyMovement();
        ApplyRotation();
        ApplyRoute();
        ApplyTimers();


        if (DebugMode)
            CheckBounds();
        
        

    }


    public void SetupBall(Level _level)
    {

        this.CurrentLevel = _level;
        this.transform.parent = _level.transform;
        this.transform.position = _level.SpawnPoint.transform.position;

       
        InputManager inputManager = FindObjectOfType<InputManager>();
        inputManager.OnMovementAdded += AddPlayerMovement;
        
    }


  //  private void ApplyFriction()
  //  {

  //      frictionTimer += Time.deltaTime;

  //      if(frictionTimer > 1f)
  //      {
  //          ChangeSpeed(-2.5f);
  //          frictionTimer = 0;
  //      }
        //experiment desaceleracion con el tiempo.
        
  //  }


    private void AddPlayerMovement(string _playerInput)
    {
        if (Jumping || Flying)
            return;

        if(_playerInput == "MoveLeft")
        {
            MovementQueue.RemoveAll(x => x == MoveRight);  //sacamos queued movement en la direccion opuesta
            MovementQueue.Add(MoveLeft);
        }
        else if(_playerInput == "MoveRight")
        {
            MovementQueue.RemoveAll(x => x == MoveLeft);  //sacamos queued movement en la direccion opuesta
            MovementQueue.Add(MoveRight);
        }
        else if(_playerInput == "Dash")
        {
             MovementQueue.RemoveAll(x => x == MoveLeft || x == MoveRight);
             MovementQueue.Add(Dash);
        }
        else
        {
            Debug.LogError(_playerInput + ": Movement not recognized");
        }
    }


    private void ApplyGravity()
    {
        if (Jumping)
            return;


        float ballRadius = sphere.Radius;

        int layerMask = 1 << 8;  //solamente layer 8 (floor);
        RaycastHit hit;
        Vector3 rayDirection = new Vector3(0, -1, 1); //diagonal enfrente abajo para anticiparse. si disparas directo para abajo arma quilombo con los floors ascendientes muy empinados.

        if (Physics.Raycast(sphere.transform.position, rayDirection, out hit, 1000f, layerMask))
        {
            CurrentFloor = hit.transform.gameObject.GetComponent<Floor>();
            CurrentBlock = CurrentFloor.parentBlock;
            CurrentFloor.ActivateBlock();
            RightEdge = CurrentFloor.RightEdge;
            LeftEdge = CurrentFloor.LeftEdge;  //guardar los valores del ultimo floor pegado, sino se pierden los valores al salirse             

            if (hit.distance > 20f && !Jumping) // la pelota esta en un jump de acantilado. ponerla en fly mode para deshabilitar giros 
            {
                if (!Flying)
                {
                    OnFlying(true);
                    Flying = true;
                    //todo: instanciar particula flying start;

                }

                gravityTimer += Time.deltaTime;
                float fallRate = gravityTimer * 3.5f;  // simula aceleracion de la caida causada por gravedad.
                float correction = Mathf.Clamp(hit.distance - ballRadius - 0.1f, 0, fallRate) * -1;
                transform.Translate(new Vector3(0, correction, 0));
            }
            else if (hit.distance > (ballRadius + 0.1f))  //la pelota esta demasiado lejos del piso, aplicar gravedad artificial
            {
                if(Flying)
                {
                    OnFlying(false);
                    Flying = false;
                    //particula flying end (ground slam) 
                    ParticleSystem particle = GameObject.Instantiate(GroundSlamVFX, this.transform.position - new Vector3(0,-ballRadius,6f), this.transform.rotation, this.transform) as ParticleSystem;
                    Destroy(particle.gameObject, 1.5f);
                }
                
                
                gravityTimer += Time.deltaTime;
                float fallRate = gravityTimer * 0.8f;  // simula aceleracion de la caida causada por gravedad.
                float correction = Mathf.Clamp(hit.distance - ballRadius - 0.1f, 0, fallRate) * -1;
                transform.Translate(new Vector3(0, correction, 0));

            }
            else if(hit.distance < (ballRadius - 0.15f))  //la pelota esta hundidad en el piso, aplicar anti-gravedad
            {
                if(Flying)
                {
                    Flying = false;
                    OnFlying(false);
                }
   
                //if (gravityTimer > 3f)  //acaba de terminar salto gigante, darle boost de velocidad
                //{
                //    ChangeSpeed(5f);
                //}    
                gravityTimer = 0;
                float correction = Mathf.Clamp( (ballRadius - hit.distance), 0, 3f);
                transform.Translate(new Vector3(0, correction, 0));
            }
            else
            {
                //esta en punto justo, no aplicar ningun correctivo este frame
                gravityTimer = 0;
            }

                
            if(DebugMode)
                Debug.DrawRay(sphere.transform.position, rayDirection * hit.distance, Color.green, 5f);

        }
        else
        {
            Debug.Log("Esto se disparo porque pusiste la pelota en algun lugar no valido (i.e demasiado hundido en el piso o fuera de los limites, y por eso el raycast no le pega an nada)");
            if(DebugMode)
                Debug.DrawRay(sphere.transform.position, rayDirection * 100f, Color.red, 5f);

            CurrentFloor = null;
        }

    }


    //esto no hace nada, solamente detecta bugs
    private void CheckBounds()
    {
            Vector3 pos = this.transform.position;
           // Debug.Log($"positionX is {pos.x} and Right edge is {RightEdge}");
         
            if (pos.x > RightEdge)
            {
                this.transform.position = new Vector3(RightEdge, pos.y, pos.z);
                Debug.LogError("you are attempting to move ball outside permited bounds");
            }

            if (pos.x < LeftEdge)
            {
                this.transform.position = new Vector3(LeftEdge, pos.y, pos.z);
                Debug.LogError("you are attempting to move ball outside permited bounds");
        }     

    }


    private void ApplyTimers()
    {
        if (hasFever)
        {
            feverTimer += Time.deltaTime;
            if(feverTimer > 10)
            {
                ExitFever();
            }
        }

       // comboTimer += Time.deltaTime;
       // if (comboTimer > 5)
       // {
       //     ResetCombo();
       // }

        if (inmunityTimer > 0)
            inmunityTimer -= Time.deltaTime;
    }

    private void ResetCombo()
    {
        comboTimer = 0;
        Combo = 0;
    }


    private void SetStageSpeed()
    {
        //TODO armar clase Stage
        switch (Stage)
        {
            case 0:
                speed = 0f;
                break;
            case 1:
                speed = 100f;
                break;
            case 2:
                speed = 125f;
                break;
            case 3:
                speed = 150f;
                break;
            case 4:
                speed = 175f;
                break;
            case 5:
                speed = feverSpeed;
                break;
            default:
                Debug.LogError(Stage + " stage not recognized");
                break;
        }
    }

    private void ApplyRotation()
    {
        FowRotContainer.transform.Rotate(new Vector3(speed * 2 * Time.deltaTime, 0f, 0f));  //rotation
    }

    private void ApplyMovement()
    {
        if (MovementQueue.Count == 0)
            MovementQueue.Add(MoveForward);

        if(route == null || route.Empty)
        {
            MovementQueue.First().Invoke();
            MovementQueue.RemoveAt(0);
        } 
    }


    private void MoveForward()
    {
        Vector3 forwardMovement = new Vector3(transform.position.x, transform.position.y, transform.position.z + speed * Time.deltaTime);
        List<Vector3> waypoints = new List<Vector3>() { forwardMovement };
        route = new Route(waypoints, Vector3.zero);
    }

    private void MoveLeft()
	{
        if (currentLane == 1)
        {
            MoveForward();
                return;
        }
        

        currentLane--;
       // Debug.Log(CurrentFloor.laneAnchors[currentLane - 1]);
        Vector3 targetPosition = new Vector3(CurrentBlock.FourLaneAnchors[currentLane -1], transform.position.y, transform.position.z + speed * 0.4f );
        Vector3 targetRotation = new Vector3(0, 0, -45);
        route = bezier.GenerateRoute(this.transform.position, targetPosition, targetRotation);


        LeftFlakes.Clear();
        LeftFlakes.Play();
    }

    private void MoveRight()
	{
        if (currentLane == 4)
        {
            MoveForward();
            return;
        }

        currentLane++;
        Vector3 targetPosition = new Vector3(CurrentBlock.FourLaneAnchors[currentLane - 1], transform.position.y, transform.position.z + speed * 0.4f);
        Vector3 targetRotation = new Vector3(0, 0, 45);
        route = bezier.GenerateRoute(this.transform.position, targetPosition, targetRotation);

        RightFlakes.Clear();
        RightFlakes.Play();

    }

    private void Dash()
    {
        Vector3 targetPosition = new Vector3(CurrentBlock.FourLaneAnchors[currentLane - 1], transform.position.y, transform.position.z + speed * 0.4f);
        route = bezier.GenerateRoute(this.transform.position, targetPosition, Vector3.zero);
        Dashing = true;
    }

    public void Fly(Route flightroute)
    {
        CentralFlakes.Clear();
        CentralFlakes.Play();
        Jumping = true;
        MovementQueue.Clear();
        route = flightroute;
    }


    private void ApplyRoute()
    {
        if (route.Empty)
            return;

        float routeSpeed;

        if (Dashing)
            routeSpeed = dashingSpeed;
        else if (Jumping)
            routeSpeed = jumpingSpeed;
        else
            routeSpeed = speed;
       
        float step = routeSpeed * Time.deltaTime * 1.5f; // magic number de aprox por ahora. deberia ser pyth: sqrtRoot( zstep^2 + lanewidth^2) / zstep

        Vector3 waypoint = route.Next();
        float temp = step - Vector3.Distance(transform.position, waypoint);  //anticipamos el remainder (antes de moverse).
        transform.position =Vector3.MoveTowards( transform.position, waypoint, step);
        step = temp;

        float rotationStep = Mathf.MoveTowardsAngle(transform.eulerAngles.y, route.Rotation.z, 3f);
        sphere.transform.Rotate(0, 0, rotationStep);


        while (step > 0 && !route.Empty)
        {
            route.Clean();  // si sobro remainder, ya se llego al waypoint anterior
            if (route.Empty)
                break;
            waypoint = route.Next();
            temp = step - Vector3.Distance(transform.position, waypoint);
            transform.position = Vector3.MoveTowards(transform.position, waypoint, step);
            step = temp;
            
        }
        
        if (route.Empty)  //se termino el route, snappear a un lane
        {
            SnapToLane();
            Dashing = false;
            Jumping = false;
            // se se termino un movement forward, instanciar snowflakes.
            if (Jumping)
                Debug.Log("Instanciar fin de salto");
        }

    }

    private void SnapToLane()
    {
        List<float> anchors = CurrentBlock.FourLaneAnchors.ToList(); //no cambiar el orden, necesitamos para sacar index original.
        float bestAnchor = anchors.OrderBy(f => Mathf.Abs(transform.position.x - f)).First();
        int anchorIndex = anchors.FindIndex(x => Math.Abs(x - bestAnchor) < 0.01);
        this.transform.position = new Vector3(bestAnchor, this.transform.position.y, this.transform.position.z);
        currentLane = anchorIndex + 1;
    }


    public void GoodHit(int points = 1)
    {
        Combo += points;
        ParticleSystem particle = GameObject.Instantiate(GoodHitVFX, this.transform.position  - 2* transform.forward, this.transform.rotation, this.transform) as ParticleSystem;
        Destroy(particle.gameObject, 1.5f);

        if (!hasFever)
        {
            Evolve();
        }

        comboTimer = 0;
        //TODO Debug.Log("Instanciar particulas good hit");
    }




    private int ComboToStageConversion(int _combo)
    {
        if (_combo >= STAGE_5_COMBO)
            return 5;
        if (_combo >= STAGE_4_COMBO)
            return 4;
        if (_combo >= STAGE_3_COMBO)
            return 3;
        if (_combo >= STAGE_2_COMBO)
            return 2;
        if (_combo >= STAGE_1_COMBO)
            return 1;
        else
            return 0;
    }

    private void Evolve()
    {
        if (Stage == 0)
            return;  //previene revivir;

        int targetStage = ComboToStageConversion(Combo);  
       if(targetStage > Stage)
       {
            ParticleSystem particle = GameObject.Instantiate(StageGrowVFX, this.transform.position, this.transform.rotation, this.transform) as ParticleSystem;
            Destroy(particle, 1.5f);

            Stage += 1;  //sube un stage a la vez.
            if (Stage == 5)
                EnterFever();
       }

    }

    private void Devolve()
    {
        if (Stage == 0)
            return;  //previene revivir.

        if(Stage == 1)
        {
            Stage = 0;
            OnDeath?.Invoke();
            StartCoroutine("LoseSequence");
                return;
        }

        Combo = Mathf.Clamp(Combo - 25, 0, 200); //penalty bad hit;
        int targetStage = ComboToStageConversion(Combo);
        Stage = targetStage;      

    }


    public void BadHit()
    {
        if (!hasFever && inmunityTimer <= 0)
        {
            Devolve(); //al pegarle a algo malo baja un stage
            inmunityTimer = 1f;
        }

    }


    public void EnterFever()
    {
        feverTimer = 0;
        //FeverParticles.SetActive(true);
        OnSwitchFever(true);
    }

    public void ExitFever()
    {
        ResetCombo();
        Combo = 0;
        Devolve();
        //FeverParticles.SetActive(false);
        OnSwitchFever(false);
    }

    private float loseTimer = 2f;
    public IEnumerator LoseSequence()
    {
        while(loseTimer > 0)
        {
            loseTimer -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        
        if(loseTimer < 0)
        {
            CurrentLevel.Lose();
            yield break;
        }
    }
   

}
