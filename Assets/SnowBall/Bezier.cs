﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Bezier : MonoBehaviour
{


    [SerializeField]
    private Transform[] controlPoints;

    private Vector3 gizmosPosition;

    private void OnDrawGizmos()
    {
        for(float t = 0; t <= 1; t += 0.05f)
        {
            gizmosPosition = Mathf.Pow(1 - t, 3) * controlPoints[0].position +
                3 * Mathf.Pow(1 - t, 2) * t * controlPoints[1].position +
                3 * (1 - t) * Mathf.Pow(t, 2) * controlPoints[2].position +
                Mathf.Pow(t, 3) * controlPoints[3].position;

            Gizmos.DrawSphere(gizmosPosition, 0.15f);
        }

        Gizmos.DrawLine(new Vector3(controlPoints[0].position.x, controlPoints[0].position.y, controlPoints[0].position.z),
        new Vector3(controlPoints[1].position.x, controlPoints[1].position.y, controlPoints[1].position.z));

        Gizmos.DrawLine(new Vector3(controlPoints[2].position.x, controlPoints[2].position.y, controlPoints[2].position.z),
        new Vector3(controlPoints[3].position.x, controlPoints[3].position.y,controlPoints[3].position.z));
    }

    // para curvas mas custom, donde los controlpoints definen la curva.
    public Route GenerateManualRoute(Transform trf, bool withAnchors = true)
    {
        float anchor;
        if(withAnchors)
        {
            float[] laneAnchors = GetComponentInParent<Block>().FourLaneAnchors;
            anchor = laneAnchors.OrderBy(f => Mathf.Abs(controlPoints[3].position.x - f)).First(); // TODO mover a game Utils
        }
        else
        {
            anchor = controlPoints[3].position.x;
        }
           

        controlPoints[0].position = trf.position;
        // contorlpoints[1],[2] => el resto de los controlpoints se usan como estan el gameObject
        controlPoints[3].position = new Vector3(anchor, controlPoints[3].position.y, controlPoints[3].position.z);
            

        List<Vector3> waypoints = new List<Vector3>();

        for (float t = 0; t < 1; t += 0.02f)  
        {
            Vector3 waypoint = Mathf.Pow(1 - t, 3) * controlPoints[0].position +
                3 * Mathf.Pow(1 - t, 2) * t * controlPoints[1].position +
                3 * (1 - t) * Mathf.Pow(t, 2) * controlPoints[2].position +
                Mathf.Pow(t, 3) * controlPoints[3].position;

            waypoints.Add(waypoint);
        }

        Route route = new Route(waypoints, Vector3.zero);
        return route;
    }



    // se usa para rutas fijas donde le pasas origen/ destino y te genera la ruta sola. 
    public Route GenerateRoute(Vector3 start, Vector3 end, Vector3 _targetRotation)
    {
            
            float Zdiff = end.z - start.z;
            controlPoints[0].position = start;
            controlPoints[1].position = start + new Vector3(0, 0, Zdiff / 2);
            controlPoints[2].position = end + new Vector3(0, 0, -Zdiff / 2);
            controlPoints[3].position = end;


        List<Vector3> waypoints = new List<Vector3>();

        for (float t = 0; t < 1; t += 0.05f)  //TODO: determinar t basado en calculo de distancia (cuando mas rapido estas llendo, mas grande t)
        {
            Vector3 waypoint = Mathf.Pow(1 - t, 3) * controlPoints[0].position +
                3 * Mathf.Pow(1 - t, 2) * t * controlPoints[1].position +
                3 * (1 - t) * Mathf.Pow(t, 2) * controlPoints[2].position +
                Mathf.Pow(t, 3) * controlPoints[3].position;

            waypoints.Add(waypoint);
        }

        Route route = new Route(waypoints, _targetRotation);
        return route;
    }

    public Route GenerateCheapRoute(Vector3 start, Vector3 end)
    {

        float Zdiff = end.z - start.z;
        float Ydiff = end.y - start.y;
        controlPoints[0].position = start;
        controlPoints[1].position = start + new Vector3(0, Ydiff/2, Zdiff / 2);
        controlPoints[2].position = end + new Vector3(0, -Ydiff/2, -Zdiff / 2);
        controlPoints[3].position = end;


        List<Vector3> waypoints = new List<Vector3>();

        for (float t = 0; t < 1; t += 0.1f)  //TODO: determinar t basado en calculo de distancia (cuando mas rapido estas llendo, mas grande t)
        {
            Vector3 waypoint = Mathf.Pow(1 - t, 3) * controlPoints[0].position +
                3 * Mathf.Pow(1 - t, 2) * t * controlPoints[1].position +
                3 * (1 - t) * Mathf.Pow(t, 2) * controlPoints[2].position +
                Mathf.Pow(t, 3) * controlPoints[3].position;

            waypoints.Add(waypoint);
        }

        Route route = new Route(waypoints, Vector3.zero);
        return route;
    }


    public Vector3 ManualLandingSpot()
    {
        return controlPoints[3].position;
    }



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
