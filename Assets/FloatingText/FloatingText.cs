﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatingText : MonoBehaviour
{
    public Sprite[] sprites = new Sprite[5];

    private float timer = 0;
    private Image image;
    private Transform following;

    private Camera gameCam;
   
    private Color FadeOutColor;
    private Color FadeInColor;

    private void Awake()
    {
        FadeInColor = new Color(1, 1, 1, 1);
        FadeOutColor  = new Color(1, 1, 1, 0);
        image = GetComponent<Image>();
        gameCam = FindObjectOfType<GameCam>().GetComponent<Camera>();
        image.enabled = false;
    }

    public void Activate(int newStage, Transform _follow)
    {
        following = _follow;
        Sprite sprite = sprites[newStage];
        image.sprite = sprite;
        image.enabled = true;
        this.transform.position = gameCam.WorldToScreenPoint(following.position);
        timer = 2f;
        StartCoroutine("FadeIn");

        if (newStage == 0)
            following = null; //todo: quitar hack
    }

    void Update()
    {
        if(timer > 0 && following != null)
        {

            timer -= Time.deltaTime;
            Vector3 ballpos = gameCam.WorldToScreenPoint(following.position);

            this.transform.position = Vector2.Lerp(transform.position, ballpos + new Vector3(0, Mathf.Min(Screen.height * -0.12f), -160f), Time.deltaTime);
            if(timer < 0)
            {
                StartCoroutine("FadeOut");
                following = null;
            }
        }
    }

    public IEnumerator FadeIn()
    {
        while (image.color.a < 0.99f)
        {
            Color newColor = Color.Lerp(image.color, FadeInColor, Time.deltaTime * 5);
            image.color = newColor;
        }
        yield return null;
    }


    public IEnumerator FadeOut()
    {
        yield return new WaitForSeconds(0.2f);
        while(image.color.a > 0.01f)
        {
            Color newColor = Color.Lerp(image.color, FadeOutColor, Time.deltaTime * 5);
            image.color = newColor;
            yield return new WaitForEndOfFrame();
        }
        yield return null;
    }
}
