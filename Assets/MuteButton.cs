﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MuteButton : MonoBehaviour
{

    public Sprite[] icons;

    private SaveManager saveManager;

    // Start is called before the first frame update
    void Start()
    {
        saveManager = SaveManager.Instance;

        GetComponent<Button>().onClick.AddListener(Toggle);

        if (saveManager.savedata.mute == false)
            this.GetComponent<Image>().sprite = icons[0];
        else
            this.GetComponent<Image>().sprite = icons[1];
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void Toggle()
    {
        AudioManager.Instance.ToggleMute();

        if (saveManager.savedata.mute == false)
            this.GetComponent<Image>().sprite = icons[0];
        else
            this.GetComponent<Image>().sprite = icons[1];

    }


}
