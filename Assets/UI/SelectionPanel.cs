﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectionPanel : Panel
{
   public ScoreKeeper scoreKeeper;

    public Button backButton;
    

   protected override void ShowInternal()
   {
        backButton.onClick.AddListener(panelController.ActivateHome);
        GetComponentInChildren<LevelButtonsController>().Refresh();

   }


    protected override void HideInternal()
    {

       backButton.onClick.RemoveAllListeners();

    }
}
 