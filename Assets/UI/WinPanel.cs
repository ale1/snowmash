﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

public class WinPanel : Panel
{
    public ScoreKeeper scoreKeeper;

    public Button BackButton;
    public Button PlayAgainButton;
    public Button NextLevelButton;

    public Text skierCount;
    public Text skierItemScore;
    public Text skierTotal;
    public Image skierFill;
    public Star skierStar;
    public Image skierMarker;

    public Text boarderCount;
    public Text boarderItemScore;
    public Text boarderTotal;
    public Image boarderFill;
    public Star boarderStar;
    public Image boarderMarker;

    public Image bikerFill;
    public Star bikerStar;
    public Image bikerMarker;

    //todo: refactor
   protected override void ShowInternal()
   {
        ResetFills();

        PlayAgainButton.onClick.AddListener(FindObjectOfType<GameManager>().RestartLevel);
        BackButton.onClick.AddListener(FindObjectOfType<GameManager>().QuitLevel);
        NextLevelButton.onClick.AddListener(FindObjectOfType<GameManager>().NextLevel);

        skierCount.text = scoreKeeper.Skiers.ItemCount.ToString();
        skierItemScore.text = "X  " + scoreKeeper.Skiers.PointsPerItem.ToString() + " = ";
        skierTotal.text = scoreKeeper.Skiers.CategoryScore.ToString();

        float skierTargetFill = scoreKeeper.Skiers.ItemCountNormalized;
        float ThresholdFill = scoreKeeper.Skiers.Threshold;
        StartCoroutine(FillRoutine(skierFill,skierTargetFill, ThresholdFill, 0.6f,OnGainSkierStar));


        boarderCount.text = scoreKeeper.Snowboarders.ItemCount.ToString();
        boarderItemScore.text = "X " + scoreKeeper.Snowboarders.PointsPerItem.ToString() + " = ";
        boarderTotal.text = scoreKeeper.Snowboarders.CategoryScore.ToString();
        float snowboarderTargetFill = (float)scoreKeeper.Snowboarders.ItemCountNormalized;
        ThresholdFill = scoreKeeper.Snowboarders.Threshold;
        StartCoroutine(FillRoutine(boarderFill, snowboarderTargetFill, ThresholdFill, 1.8f,OnGainBoarderStar));


        float snowBikerTargetFill = (float)scoreKeeper.SnowBikers.ItemCountNormalized;
        ThresholdFill = scoreKeeper.SnowBikers.Threshold;
        StartCoroutine(FillRoutine(bikerFill, snowBikerTargetFill,ThresholdFill,3f,OnGainBikerStar));

        skierMarker.transform.Translate(new Vector3(skierFill.rectTransform.rect.width * scoreKeeper.Skiers.Threshold , 0f, 0f),Space.World);  
        boarderMarker.transform.Translate(new Vector3(boarderFill.rectTransform.rect.width * scoreKeeper.Snowboarders.Threshold, 0, 0), Space.World);  //
        bikerMarker.transform.Translate(new Vector3(bikerFill.rectTransform.rect.width * scoreKeeper.SnowBikers.Threshold, 0, 0), Space.World);  // 
    }


    protected override void HideInternal()
    {
        PlayAgainButton.onClick.RemoveAllListeners();
        NextLevelButton.onClick.RemoveAllListeners();
        ResetMarkers();
    }


    private void ResetMarkers()
    {
        skierMarker.transform.localPosition = new Vector3(0, 0, 0);
        boarderMarker.transform.localPosition = new Vector3(0, 0, 0);
        bikerMarker.transform.localPosition = new Vector3(0, 0, 0);

    }

    //todo: refactor
    private void ResetFills()
    {
        skierFill.color = Color.white;
        skierFill.fillAmount = 0;
        if(scoreKeeper.Skiers.StarFilled)
        {
            skierStar.Fill();
        }
        else
        {
            skierStar.Empty();
        }

        boarderFill.color = Color.white;
        boarderFill.fillAmount = 0;
        if (scoreKeeper.Snowboarders.StarFilled)
        {
            boarderStar.Fill();
        }
        else
        {
            boarderStar.Empty();
        
        }

        bikerFill.color = Color.white;
        bikerFill.fillAmount = 0;
        if (scoreKeeper.SnowBikers.StarFilled)
        {
            bikerStar.Fill();
        }
        else
        {
            bikerStar.Empty();
        }

     

    }

    private void OnGainSkierStar()
    {
        //todo: desacoplar starcount del UI;

        if (skierStar.filled == false)
        {
            skierStar.Fill();
            skierFill.color = Color.green;
        }
    }


    private void OnGainBoarderStar()
    {
        if (boarderStar.filled == false)
        {
            boarderStar.Fill();
            boarderFill.color = Color.green;
        }
    }

    private void OnGainBikerStar()
    {
        if (bikerStar.filled == false)
        {
            bikerStar.Fill();
            bikerFill.color = Color.green;
        }
    }



    IEnumerator FillRoutine(Image imageFill, float targetFill, float _thresholdFill, float _delay, Action OnGainStar )
    {
     
        yield return new WaitForSeconds(_delay);
        while (imageFill.fillAmount < targetFill)
        {
            imageFill.fillAmount += Mathf.Min(targetFill - imageFill.fillAmount , 0.01f);
            if (imageFill.fillAmount > _thresholdFill ) 
                OnGainStar();
            yield return new WaitForSeconds(0.01f);
        }
        yield break;
    }


}


