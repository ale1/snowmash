﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class energyball : MonoBehaviour
{
    public bool activated { get; private set; }
    private Image image;

    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
        activated = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void Activate()
    {
        activated = true;
        image.enabled = true;
    }

    public void Deactivate()
    {
        activated = false;
        image.enabled = false;
    }

    public void Colorize(Color _color)
    {
        GetComponent<Image>().color = _color;
    }
}
