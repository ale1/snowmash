﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HomePanel : Panel
{
    public Button StartButton;
    public Button LevelSelectButton;


    protected override void ShowInternal()
    {
       StartButton.onClick.AddListener(FindObjectOfType<GameManager>().StartGame);
       LevelSelectButton.onClick.AddListener(panelController.ActivateLevelSelect);
    }


    protected override void HideInternal()
    {

        StartButton.onClick.RemoveAllListeners();
        LevelSelectButton.onClick.RemoveAllListeners();

    }
}
