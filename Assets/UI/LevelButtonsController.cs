﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class LevelButtonsController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
               
    }


    public void Refresh()
    {
        LevelButton[] buttons = GetComponentsInChildren<LevelButton>();
        for(int i = 0; i < buttons.Length; i++)
        {
            buttons[i].id = i+1;
            buttons[i].Refresh();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    
}
