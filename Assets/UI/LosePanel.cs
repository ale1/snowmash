﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LosePanel : Panel
{
   public ScoreKeeper scoreKeeper;


    public Button BackButton;
    public Button ReplayButton;



    protected override void ShowInternal()
    {
        PanelController panelController = GetComponentInParent<PanelController>();
        BackButton.onClick.AddListener(panelController.ActivateHome);
        ReplayButton.onClick.AddListener(FindObjectOfType<GameManager>().RestartLevel);
   }


    protected override void HideInternal()
    {

        BackButton.onClick.RemoveAllListeners();
        ReplayButton.onClick.RemoveAllListeners();
    }
}
