﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Constants.GameVars;

public class EnergyBar : MonoBehaviour
{
    public int FixedStage; // set by inspector

    public int roof;

    public int currentCombo;
    private Image image;


    // Start is called before the first frame update
    void Start()
    {
        roof = CombosPerStage(FixedStage);
        image = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Refresh(int _combo)
    {
        currentCombo = _combo;
        float value = (float)Mathf.Min(_combo, roof);
        image.fillAmount = value / (float) CombosPerStage(4);
    }

    public void Reset()
    {
        currentCombo = 0;
        Refresh(currentCombo);
    }


    private int CombosPerStage(int _stage)
    {
        switch (_stage)
        {
            case 0:
                return 0;
            case 1:
                return STAGE_2_COMBO;
            case 2:
                return STAGE_3_COMBO;
            case 3:
                return STAGE_4_COMBO;
            case 4:
                return STAGE_5_COMBO;
            case 5:
                return MAXCOMBO;
            default:
                Debug.LogError("something went wrong with:" + _stage);
                return -1;
                
        }
    }
}
