﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Constants.GameVars;

public class EnergyBallController : MonoBehaviour
{

    public Color[] colors = new Color[5];

    private bool feverMode;
    public energyball[] Energyballs;

    private Snowball snowball;



    // Start is called before the first frame update
    void Start()
    {
        Energyballs = GetComponentsInChildren<energyball>(); 
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Restart(Snowball _snowball)
    {
        snowball = _snowball;
        Energyballs = GetComponentsInChildren<energyball>();
        feverMode = false;
        Refresh(0);
        snowball.OnSwitchFever += FeverMode;
    }


    public void Refresh(int _combo)
    {
        if (feverMode)
            return;
        foreach(energyball ball in Energyballs)
        {
            ball.Deactivate();
        }


        //for each stage
        for(int i=1; i < 5; i++)
        {
       
            int comboPointsavailable = Mathf.Min(_combo - CombosPerStage(i - 1),CombosPerStage(i));
            int count = (int) (comboPointsavailable / 5);
            //Debug.Log(comboPointsavailable +"---" + count);

            if(count <= 0)
                continue;

            foreach(energyball ball in Energyballs)
            {
                if(count > 0 && ball.activated == false )
                {
                    ball.Activate();
                    ball.Colorize(colors[i-1]);
                    count--;
                }
                else
                    continue;
            }
        }

      

    }


    private int CombosPerStage(int _stage)
    {
        switch (_stage)
        {
            case 0:
                return 0;
            case 1:
                return STAGE_2_COMBO;
            case 2:
                return STAGE_3_COMBO;
            case 3:
                return STAGE_4_COMBO;
            case 4:
                return STAGE_5_COMBO;
            case 5:
                return MAXCOMBO;
            default:
                Debug.LogError("something went wrong with:" + _stage);
                return -1;

        }
    }


    private void FeverMode(bool _activated)
    {
        feverMode = _activated;

        if(_activated)
        {
            foreach(energyball ball in Energyballs)
            {
                ball.Colorize(colors[4]);
            }
        }


    }
}
;