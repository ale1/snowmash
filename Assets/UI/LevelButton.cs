﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
    public int id;


    //local vars
    private Button button;
    [SerializeField] private Image lockIcon;
    private Star[] stars;

    private void Awake()
    {
    #if UNITY_EDITOR
        if (lockIcon == null)
            Debug.LogError("SalameException:: te olvidaste de setear lockIcon a mano");
    #endif
    }


    // Start is called before the first frame update
    void Start()
    {
        stars = GetComponentsInChildren<Star>(true);
        button = GetComponentInChildren<Button>();
        button.onClick.AddListener(LoadLevel);

        Refresh();

    }

    public void Refresh()
    {

        GetComponentInChildren<Text>().text = id.ToString();

        if(button.interactable == false)
        {
            lockIcon.gameObject.SetActive(true);
        }
        else
        {
            lockIcon.gameObject.SetActive(false);
        }

        ScoreKeeper sk;
       if(SaveManager.Instance.savedata.scoreKeepers.TryGetValue(id,out sk))
       {
            List<ScoreCategory> scoreCats = sk.ScoreCats;
            for (int i = 0; i < scoreCats.Count; i++)
            {
                if (scoreCats[i].StarFilled == true)
                    stars[i].Fill();
                else
                    stars[i].Empty();
            }

        }


    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void LoadLevel()
    {
        GameManager GM = FindObjectOfType<GameManager>();
        GM.SelectedLevel = this.id;
        GM.StartGame();
    }
}
